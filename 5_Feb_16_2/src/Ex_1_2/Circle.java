package Ex_1_2;

public class Circle {
    private double radius;
    private String color;

    //Constructs a Circle instance with default value for radius and color
    public Circle(){
        radius = 1.0;
        color = "red";
    }

    // Constructs a Circle instance with the given radius and default color
    public Circle(double r){
        radius = r;
        color = "red";
    }

    public double getRadius(){
        return radius;
    }

    public void setRadius(double r){
        radius = r;
    }

    public double getCircumference(){
        return 2 * Math.PI * radius;
    }

    public double getArea(){
        return Math.PI * radius * radius;
    }

    public String toString(){
        return "Circle[radius=" + radius + ",color=" + color + "]";
    }
}
