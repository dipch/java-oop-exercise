package Ex_2_5;


public class TestMain {
    public static void main(String[] args) {
        Customer c1 = new Customer(1,"dip", 'm');
        Customer c2 = new Customer(2, "pid", 'f');
        Account a1 = new Account(1, c1, 580);
        System.out.println(c1);  // toString();
        Account a2 = new Account(2, c2, 1200); // default balance
        System.out.println(a2);

        // Test Getters
        System.out.println("ID: " + a1.getID());
        System.out.println(a1.getCustomerName());
        System.out.println("Balance: " + a1.getBalance());
        a1.deposit(23);
        System.out.println(a1);
        a1.withdraw(250);
        System.out.println(a1);
    }
}