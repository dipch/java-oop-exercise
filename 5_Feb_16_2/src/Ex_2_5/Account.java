package Ex_2_5;

public class Account {
    private int id;
    private Customer customer;
    private double balance = 0.0;

    public Account(int id, Customer customer, double balance) {
        this.customer = customer;
        this.balance = balance;
        this.id = id;
    }

    public Account(int id, Customer customer){
        this.customer = customer;
        this.balance = balance;
    }

    public int getID() {return id;}
    public Customer getCustomer() {return customer;}
    public double getBalance() {return balance;}
    public void setBalance(double balance) {this.balance = balance;}
    public  String toString() {
        return customer.getName() + "(" + id+ ") balance=$" + String.format( "%.2f", balance);
    }

    public String getCustomerName() {
        return  customer.getName();
    }
    public Account deposit(double amount){
        balance += amount;
        return this;
    }
    public Account withdraw(double amount){
        if(balance >= amount){
            balance-=amount;
        }
        else
            System.out.println("amount withdrawn exceeds the current balance!");

        return this;
    }





}
