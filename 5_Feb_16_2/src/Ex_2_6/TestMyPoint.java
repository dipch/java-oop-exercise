package Ex_2_6;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint(2, 3);
        MyPoint p2 = new MyPoint(5, 7);
        System.out.println(p1.toString());
        System.out.println(p2.toString());
        System.out.println(p1.distance(p2));
        System.out.println(p1.distance(5, 6));  // which version?
        System.out.println(p1.distance());      // which version?
        p1.setX(8);   // Test setters
        p1.setY(6);
        System.out.println("x is: " + p1.getX());  // Test getters
        System.out.println("y is: " + p1.getY());
        p1.setXY(3, 0);   // Test setXY()
        System.out.println(p1.getXY()[0]);  // Test getXY()
        System.out.println(p1.getXY()[1]);
        System.out.println(p1);


        MyPoint[] points = new MyPoint[10];  // Declare and allocate an array of MyPoint
        for (int i = 0; i < points.length; i++) {
            points[i] = new MyPoint(i+1,i+1);
            // Allocate each of MyPoint instances
        }
        for (int i = 0; i < points.length; i++) {
            System.out.println(points[i].toString());
        }



    }
}
