package Ex_2_7;

public class MyLine {
    MyPoint begin;
    MyPoint end;

    MyLine(int x1, int y1, int x2, int y2) {
        this.begin = new MyPoint(x1, y1);
        this.end = new MyPoint(x2, y2);
    }
    MyLine(MyPoint begin, MyPoint end) {
        this.begin = begin;
        this.end = end;
    }
    public void setBegin(MyPoint begin) {
        this.begin = begin;
    }
    public void setEnd(MyPoint end) {
        this.end = end;
    }
    public MyPoint getBegin() {
        return this.begin;
    }
    public MyPoint getEnd() {
        return this.end;
    }
    public int getBeginX() {
        return this.begin.getX();
    }
    public void setBeginX(int beginX) {
        this.begin.setX(beginX);
    }
    public int getBeginY() {
        return this.begin.getY();
    }
    public void setBeginY(int beginY) {
        this.begin.setY(beginY);
    }
    public int getEndX() {
        return this.end.getX();
    }
    public void setEndX(int endX) {
        this.end.setX(endX);
    }
    public int getEndY() {
        return this.end.getY();
    }
    public void setEndY(int endY) {
        this.end.setY(endY);
    }
    public int[] getBeginXY() {
        return new int[] {this.begin.getX(), this.begin.getY()};
    }
    public int[] getEndXY() {
        return new int[] {this.end.getX(), this.end.getY()};
    }
    public void setBeginXY(int x, int y) {
        this.begin.setXY(x, y);
    }
    public void setEndXY(int x, int y) {
        this.end.setXY(x, y);
    }
    public double getLength() {
        //return this.begin.distance(this.end);
        return Math.sqrt(Math.pow(this.begin.getX() - this.end.getX(), 2) + Math.pow(this.begin.getY() - this.end.getY(), 2));
    }
    public double getGradient() {
        return Math.atan2(this.begin.getY(), this.end.getY()) - Math.atan2(this.begin.getX(), this.begin.getX());
    }

    public String toString() {
        return "MyLine[begin=(" + this.begin.getX() + ", " + this.begin.getY() + "),end=(" + this.end.getX() + "," + this.end.getY() + ")]";
    }


}
